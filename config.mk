#
# Copyright (C) 2021 The Android Open Source Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Automated
$(call inherit-product, vendor/google/gms/gms-vendor.mk)

# Overlays
PRODUCT_PACKAGES += \
    GmsCarrierConfigOverlay \
    GmsConfigOverlayCommon \
    GmsConfigOverlayComms \
    GmsConfigOverlayGeotz \
    GmsConfigOverlayGSA \
    GmsConfigOverlayPhotos \
    GmsContactsProviderOverlay \
    GmsSettingsOverlay \
    GmsSettingsProviderOverlay \
    GmsSetupWizardOverlay \
    GmsSystemUIOverlay \
    GmsTelecommOverlay \
    GmsTelephonyOverlay

# Packages
PRODUCT_PACKAGES += \
    PartnerBookmarksProvider
